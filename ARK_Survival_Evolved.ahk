#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#IfWinActive, ARK: Survival Evolved
#MaxThreadsPerHotkey 2

;Rough Rider Control

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Variable Definitions
;toggleRun :=! ToggleRun
SetControlDelay -1
CoordMode, Mouse, Screen

toggleWhistle		:= 0
toggleClick 		:= 0
toggleSClick 		:= 0
toggleRun 		:= 0
toggleTransfer		:= 0
toggleConsumption 	:= 0

ConsumptionTimer	:= (1 * 60 * 1000)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Launch the Nvidia Control Panel in the background if it is not currently running.
Process, Exist, nvcplui.exe; check to see if the Nvidia Control Panel is already opened

if (ErrorLevel = 0) ; If it is not running
{
	Run, %A_ProgramFiles%\NVIDIA Corporation\Control Panel Client\nvcplui.exe, , Min;
}
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Food and Water Timer
;; Food and Water items must be in first and second slots

; FoodAndWaterTimer:
; Critical
	; ControlSend, , 1, ARK: Survival Evolved
	; ControlSend, , 2, ARK: Survival Evolved
; Return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#MaxThreadsPerHotkey 2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Local Chat override
; Control Enter for Local Chat

^Enter::
	Send {Insert down}{Insert up}
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Tribe Chat override
; Shift Enter for Tribe Chat
+Enter::
	Send {/ down}{/ up}
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Auto-Walk
;; Press F1 to toggle
F1::
	ToggleRun :=! ToggleRun
	
	if (!ToggleRun)
	{
		ControlSend,,{w down},ahk_class UnrealWindow
	}
	else
	{
		ControlSend,,{w up},ahk_class UnrealWindow
	}
Return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Auto-Gather 
;; Press F2 to toggle
F2::
	toggleGather :=! toggleGather
	
	Loop
	{
		if (!toggleGather)
		{
			Break
		}
		
		Send {e}
		sleep 500
	}
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Auto-Stay Whistle
;; Press F3 to toggle
F3::
	toggleWhistle :=! toggleWhistle
	

	While toggleWhistle {
		ControlSend, , u, ARK: Survival Evolved

		sleep 1000
    }
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Auto-Transfer 
;; Press F7 to toggle
F8::
	toggleTransfer :=! toggleTransfer
	
	; SetTimer, FoodAndWaterTimer, %ConsumptionTimer%
	
	Loop
	{
		if (!toggleTransfer)
		{
			; SetTimer, FoodAndWaterTimer, Off
			Break
		}
		
		Send {t}
		sleep 500
	}
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Auto-Click
;; Press Shift+LClick to toggle
+^LButton::
	toggleClick :=! toggleClick

	While toggleClick {
		ControlClick, x0 y0, ARK: Survival Evolved, , , , NA POS

		sleep 200
    }
return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Pin Code Brute Force
;; Press F4 to begin
;F4:
;	toggleBruteForce :=! toggleBruteForce
;	SetFormat, Float, 04.0
;	var:="0000"

	; Loop, 9999
	; {
		; if (!toggleBruteForce)
		; {
			; Break
		; }
		
		; Send e
		; Sleep, 760

		; Send %var%
		
		; FileAppend, %var%`n, %A_ScriptDir%\Brute_Force_Attempts.txt
		
		; var+=1.0

		; Sleep, 10000 ; Sleep for 10 seconds between tries
	; }
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; nVidia Brightness Controls
;;
!NumpadAdd:: ; Alt + Numpad plus.
    ControlSend, msctls_trackbar323, {Right 10}, NVIDIA Control Panel
Return

!NumpadSub:: ; Alt + Numpad minus.
    ControlSend, msctls_trackbar323, {Left 10}, NVIDIA Control Panel
Return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#IfWinActive
